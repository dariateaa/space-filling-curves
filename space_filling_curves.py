import turtle


class Curve:
    def __init__(self, alpha, angle, axiom, replacement_rules, go, order):
        self.Order = order
        self.Alpha = alpha
        self.Angle = angle
        self.Axiom = axiom
        self.Replacement_rules = replacement_rules
        self.go = go

    def draw(self):
        res = self.Axiom
        for a in range(self.Order):
            order_str = ''
            for c in res:
                if c in self.Alpha:
                    order_str += self.Replacement_rules[c]
                else:
                    order_str += c
            res = order_str

        dis = 100.0/(2**self.Order)

        for c in res:
            if c in self.go:
                turtle.forward(dis)
            if c is '-':
                turtle.left(self.Angle)
            if c is '+':
                turtle.right(self.Angle)
        turtle.done()


SERPINSKY_CURVE = [['A', 'G'], 120, 'A-G-G',
                {
                    'A': 'A-G+A+G-A',
                    'G': 'GG'
                }, ['A', 'G']]

GILBERT_CURVE = [['A', 'B'], 90, 'A',
                  {
                      'A': '-BF+AFA+FB-',
                      'B': '+AF-BFB-FA+'
                  }, ['F']]

GOSPER_CURVE = [['A', 'B'], 60, 'A',
                {
                    'A': 'A-B--B+A++AA+B-',
                    'B': '+A-BB--B-A++A+B'
                }, ['A', 'B']]

choose = int(input("1. Треугольник Серпинского \n2. Кривая Гильберта \n3. Кривая Госпера \nВаш выбор:"))
order = int(input("Введите порядок кривой: "))

if choose == 1:
    t = Curve(SERPINSKY_CURVE[0], SERPINSKY_CURVE[1], SERPINSKY_CURVE[2], SERPINSKY_CURVE[3], SERPINSKY_CURVE[4], order)
    t.draw()
elif choose == 2:
    gilbert = Curve(GILBERT_CURVE[0], GILBERT_CURVE[1], GILBERT_CURVE[2], GILBERT_CURVE[3], GILBERT_CURVE[4], order)
    gilbert.draw()
elif choose == 3:
    gosper = Curve(GOSPER_CURVE[0], GOSPER_CURVE[1], GOSPER_CURVE[2], GOSPER_CURVE[3], GOSPER_CURVE[4], order)
    gosper.draw()
